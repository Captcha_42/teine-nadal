﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Konstruktorid
{
    class Program
    {
        static void Main(string[] args)
        {
            new Inimene("355040098844") { Nimi = "Henn"};
            new Inimene("355040098844", "Ants");

            Console.WriteLine(Inimene.Leia("355040098844").Nimi);

            Inimene.Create("355040098844").Nimi);
        }
    }

    class Inimene
    {
        public static Dictionary<string, Inimene> Inimesed = new Dictionary<string, Inimene>();

        static int loendur;

        public int Nr { get; } = ++loendur;

        public string Nimi { get; set; }

        public readonly string IK;

        //public string IK { get; private set;}
        /*
        public Inimene (string ik)
        {
            IK = ik;
        }
        */
        public Inimene(string ik)
        {
        IK = ik;
            Inimesed.Add(ik, this);
        }
        public Inimene(string ik, string nimi)              // overloaditud constructor -  erinevad üksteisest parameetrite arvu, suuna, tüübi poolest. 
            : this(ik)                                      // konstruktorite sidumine
        {
            Nimi = nimi;
        }
        public static Inimene Leia(string ik) => Inimesed.ContainsKey(ik) ? Inimesed[ik] : null;  // leia
       
        public static Inimene Create(string ik, string nimi = "")
        {
            if (Inimesed.ContainsKey(ik)) return Inimesed[ik];
            else return new Inimene(ik, nimi);
        }


    }
}
