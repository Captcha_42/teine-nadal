﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klassid_1
{
    class Program
    {
        static void Main(string[] args)
        {
            // vt. muutja
            // mälus mingi piirkond, kus andmeid on võimalik hoida
            // mida need bitid baidid tähendavad
            // võimalikud väärtused
            // mida on võimalik muutujaga teha

            Inimene sander = new Inimene();
            sander.Nimi = "Sander Paljak";
            sander.Vanus = 30;

            Inimene ants = new Inimene() { Nimi = "Ants Saunamees", Vanus = 80 }; // eelistatud kirjaviis
            
            Inimene teine = sander;   //omistamine muutuja ja avaldis sama tüüpi 
            teine.Nimi = "Sarvik";    // classi tüüpi muutujal on sees oma väärtuse aadress
                                      // muutuja omistamisel teisele muutujale kopeeritakse aadress
                                      // Inimene teine = henn; sellel omistamisel kopeeritakse aadress 
                                      // struct eruneb classist, selle omistamisel kopeeritakse aadress 
                                      // class on reference tüüpi asi, muutujas on aadress
                                      // struct on value tüüpi väärtus, muutujas on väärtus
                                      // class (struct) on definitsioon
                                      // Objekt on konkreetne eksemplar, mis vastab sellele definitsioonile - Inimene on class, sander on objekt selles klassis
                                      // classi definitsioon ütleb millest objekt koosneb ja mida temaga teha saab - Data(mudel); funktsionaalsus - mida temaga teha saab


            List<Inimene> rahvas = new List<Inimene>
            {
                new Inimene{Nimi = "Joosep", Vanus = 118},
                new Inimene{Nimi = "Teele"},
                new Inimene{Nimi = "Arno"},
                new Inimene{Nimi = "Peedu"},
                sander,
                ants,
                new Inimene {Nimi = "Maris", Kaasa = sander},

            };

            Inimene[] inimesed = new Inimene[10];       // kümme inimest Array
            List<Inimene> teised = new List<Inimene>(); // kümme inimest Listis

            teised.Add(new Inimene { Nimi = "Ants", Vanus = 28 });
            teised.Add(new Inimene { Nimi = "Peeter" });
            teised.Add(sander);
            sander.Kaasa = new Inimene { Nimi = "Kadri", Kaasa = sander };
            foreach (var x in inimesed) 
            {
                Console.WriteLine(x.Kaasa?.Nimi?? "kaasa puudub");
            }

        }
        class Inimene // andmemudel - lihtsustis
        {
            public string Nimi;   // vaikimisi tühi string                                      
            public int Vanus = 18;     // vaikimisi 0                   zero - väärtus 0
            public Inimene Kaasa; // vaikimisi kandiline null e. null - tühi väärtus

            // funktsionaalsus - mis tehtud selles klassis on
            // klasside defnitsioonide jada
            // klasside-definitsioonide järjekord ei oma tähtsust
            // klasside nimed peavad olema kirjeldavad
            // klasse võivad olla ühes failis mitu tükki
            public override string ToString() => ($"inimene{Nimi} vanusega {Vanus}");                   // funktsioon + väljad - (class member)
                                                                                                        // objekti väljad sisaldavad väärtust(object data, object state)
                                                                                                        // classi liikmed on: väljad funktsioonid, meetodid, propertid, indexerid, eventid, operaatorid, konstruktorid
                                                                                                        // peaaegu kõiki kasutame ja oskame!
            

            }
        }
    }
}
