﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harjutus_3
{

    enum Gender { Naine, Mees }
    enum ChildGender { Tüdruk, Poiss }
    class Person
    {
        public string Nimi { get; set; }
        public string IK { get; set; }


        public Gender Gender => (Gender)(IK[0] % 2);

        public ChildGender ChildGender => (ChildGender)(IK[0] % 2);
        public string GenderStr => Age >= 20 ? Gender.ToString() : ChildGender.ToString();
        public DateTime BirthDay =>
            IK == "" ? new DateTime() :
            new DateTime(
                (IK[0] == '5' || IK[0] == '6' ? 2000:
                 IK[0] == '3' || IK[0] == '4' ? 1900:
                 1800) +
                int.Parse(IK.Substring(1, 2)),
                int.Parse(IK.Substring(3, 2)),
                int.Parse(IK.Substring(5, 2))


                );
        public int Age => (DateTime.Today - BirthDay).Days * 4 / 1461; // (365 * 4 + 1) 
                                                                       // static - ühine nimetaja kõikidele elementidele klassis

        public int VanusO() => (DateTime.Today - this.BirthDay).Days * 4 / 1461; // (365 * 4 + 1) 
                                                                                // static - ühine nimetaja kõikidele elementidele klassis
        public static int Vanus(Person p) => (DateTime.Today - p.BirthDay).Days * 4 / 1461; // (365 * 4 + 1) 
                                                                                // static - ühine nimetaja kõikidele elementidele klassis

        public override string ToString()
        => $"{GenderStr} {Nimi} vanusega {Age} a ";


    }
}
