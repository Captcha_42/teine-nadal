﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Harjutus_3
{
    class Program
    { 
        static void Main(string[] args)
        {
                        // kui paned muutujale nimesid - kõik mis on ainsus, nendele pannakse nimed ainsuses, mitmuses siis mitmuses.
            //
            List<Person> people = new List<Person>();

            ReadPeople("..\\..\\Õpetajad.txt", people);
            ReadPeople("..\\..\\Õpilased.txt", people);

            Console.WriteLine("\nJärgmisel kuu on sünnipäev\n");
            foreach (var p in people)
                if (p.BirthDay.Month == DateTime.Now.Month + 1)
                Console.WriteLine(p);

            // kodus millal järgmine nädal kellegil sünnipäev on: 
            // today - today.Dayofweek +
            // +7
            // -14
            Person henn = new Person();
            Console.WriteLine(henn.VanusO());
            Console.WriteLine();
        }
        
        static void ReadPeople(string filename, List<Person> list)
        {
            
            foreach (var rida in File.ReadAllLines(filename))
            { 
            var osad = rida.Split(',');
            if ( osad.Length > 1)
            list.Add(new Person{IK = osad[0].Trim(), Nimi = osad[1].Trim()});
            }
        }
    }
    
}
