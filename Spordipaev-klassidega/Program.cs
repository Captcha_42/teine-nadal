﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Spordipaev_klassidega
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"..\..\spordipäeva protokoll.txt";
            var loetudread = File.ReadAllLines(filename);

            List<Tulemus> protokoll = new List<Tulemus>();
            Dictionary<int, Distants> distantsid = new Dictionary<int, Distants>();

            string kiireimJooksja = "";
            double suurimKiirus = 0;
            
            
            for (int i = 1; i < loetudread.Length; i++)
            {
                var rida = loetudread[i].Split(',');
                string nimi = rida[0];
                int distants = int.Parse(rida[1]);
                double aeg = double.Parse(rida[2]);
                double kiirus = distants / aeg;

                protokoll.Add(new Tulemus
                {
                    Nimi = nimi,
                    Distants = distants,
                    Aeg = aeg,
                    Kiirus = kiirus,
                });
                if (kiirus > suurimKiirus) (suurimKiirus, kiireimJooksja) = (suurimKiirus, nimi);
                if (!distantsid.ContainsKey(distants)) distantsid.Add(distants, new Distants
                {
                    Pikkus = distants,
                    Parim = nimi,
                    Kiirus = kiirus,
                }
                );
                if (kiirus > distantsid[distants].Kiirus)
                {
                    distantsid[distants].Parim = nimi;
                    distantsid[distants].Kiirus = kiirus;
                }
            }
                Console.WriteLine("\n protokoll \n");
                foreach (var p in protokoll) Console.WriteLine(p);

                Console.WriteLine("\n distantside paremad \n");
                foreach (var d in distantsid) Console.WriteLine(d.Value);

                Console.WriteLine("\n kiireim jooksja \n");
                Console.WriteLine($"{kiireimJooksja} kiirusega {suurimKiirus:F2}");
            }
        }
        class Tulemus
        {
            public string Nimi;
            public double Aeg;
            public double Kiirus;
            public int Distants;
            public override string ToString() => $"{Nimi} jooksis {Distants} ajaga {Aeg:F2} kiirusega {Kiirus: F2}";
        }
        class Distants
        {
            public int Pikkus;
            public double Kiirus;
            public string Parim;
            public override string ToString() => $"distantsi {Pikkus} parim oli {Parim} kiirusega {Kiirus: F2}";
        }
    }

