﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maksud
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Inimene> inimesed = new List<Inimene>
            {
            new Inimene { Nimi = "Henn", Palk = 10000 },
            new Inimene { Nimi = "Ants", Palk = 10000 },
            new Inimene { Nimi = "Peeter", Palk = 8000},
            };
            foreach (var i in inimesed) Console.WriteLine($"{i.Nimi} saab {i.Palk - i.TuluMaks()} raha");
            {

            }
        }
    }

   
    class Inimene
    {
        public string Nimi { get; set; }                            // igal inimesel oma nimi

        public decimal Palk { get; set; }                           // oma palk

        public decimal TuluMaks() => Maks(Palk);


        public static decimal Maksuvaba { get; set; } = 500;        // kõigil sama maksuvaba tulu

        public static decimal Maksumäär { get; set; } = 0.2M;       // kõigil sama maksumäär

        public static decimal Maks(decimal summa) => summa < Maksuvaba ? 0 : (summa - Maksuvaba) * Maksumäär;

    }

}

