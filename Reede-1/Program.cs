﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reede_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("ma tahaks jagamisega tegeleda");

            try
            {
                Console.WriteLine("anna üks number");
                int yks = int.Parse(Console.ReadLine());

                Console.WriteLine("anna teine number");
                int teine = int.Parse(Console.ReadLine());

                if (teine == 0) throw new Exception("nulliga ei viitsi");
                int tulemus = yks / teine;

                Console.WriteLine(tulemus);

            }
            catch (Exception e)
            {
                Console.WriteLine("Jou");
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                Console.WriteLine("teema maas")
            }
            
        }
    }
}
