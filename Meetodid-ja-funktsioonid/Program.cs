﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meetodid_ja_funktsioonid
{
    class Program
    {           //static - väljad ilma kirjeta static on kokku 1 kasutatakse siis kui väli on universaalne ühine nimi. nt. igal inimesel on nimi ,aga järjekorra nr on ühine
                //static - 
                //public - class member access level
                //1.public - igalt poolt kättesaadav
                //2.protected - kättesaadav oma klassi ja sellest klassist tuletatud uute klasside sees.
                //3.internal - kättesaadav kõigis klassides, mis on programmi sees. programmisiseselt
                //4.private - ainult selle classi sees

        // kui ei ole öeldud - siis on private
        // class & struct ise on vaikimisi internal

        static void Main(string[] args)
        {
            /*
            DateTime sünnipäev = new DateTime(1995, 3, 7);
            //var vanus = (DateTime.Today - sünnipäev).Days*4/1461;
            //Console.WriteLine(Vanus(sünnipäev,"n"));
            Console.WriteLine(Inimene.Vanus(sünnipäev));                // funktsiooni kasutamine(avaldises)
            Trüki(sünnipäev);

            if (Inimene.Vanus(sünnipäev) > 18) Console.WriteLine("Talle võib viina müüa");
            else Console.WriteLine("Mine koju");

            if (Inimene.Vanus(sünnipäev) > 18) Console.WriteLine("Talle võib viina müüa");
            */
            Inimene henn = new Inimene { Nimi = "Henn", Sünniaeg = new DateTime(2005, 3, 8) };
            Inimene ants = new Inimene { Nimi = "Ants", Sünniaeg = DateTime.Parse("17. aprill 2000") };
            Console.WriteLine(henn);
            Console.WriteLine(ants);
        } 
        
        List<Inimene> kahekesi = new List<Inimene>
        {
            henn, ants,
        };
        foreach (var x in kahekesi x.Ageinstants();
        FormatException(int = 0;i<>)
        {

        }

        
        static void Trüki(DateTime päev)    // meetod - teeb midagi - meetodil on vastuse tüüp void
                                            // meetodit kutsume välja
        {
            Console.WriteLine(päev.ToString("(ddd) dd.MMM.yyyy"));
            return; // see võib ka puududa


            /*static void Trüki(DateTime päev)                      // kui meetod koosneb ühest lausest mis ei ole plokk on meetod lubatud kirjutada
            => Console.WriteLine(päev.ToString("(ddd) dd.MMM.yyyy"));
            */
        }
        class Inimene
        {
            //internal / public
            public static int Vanus(DateTime päev)     //, string sugu) //funktsioon-arvutab midagi ja annab vastuse - fn'l on andmetüüp
                                                       // funktsioon - antakse ette parameetrid/ muutjad, mis saavad väärtuse, kui selle poole pöördutakse
            {
                //if (sugu.Substring(0, 1) == "n") return 25;
                //kasutame seal kus on vaja midagi arvutada
                var v = (DateTime.Today - päev).Days * 4 / 1461;
                return v;
            }
            public string Nimi;
            public DateTime Sünniaeg;
            static int inimesteArv = 0;
            public int Number = ++inimesteArv;
            public override string ToString()
        => $"{Number}. {Nimi} kes on sündinud {Sünniaeg:dd.MM.yyyy}";



            public int Age() => Vanus(this.Sünniaeg);

            public static int Vanus(DateTime päev);
        }
            

    }
}
