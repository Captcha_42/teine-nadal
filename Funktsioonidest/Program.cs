﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funktsioonidest
{
    static class MyFunctions
    {
        public static int Liida(int a, int b)
        {
            return a + b;
        }
        public static void Tryki(int a, string nimi)
        {
            Console.WriteLine($"{nimi} : {a}");
        }
        public static void Arvuta(int x, int y, out int summa, out int korrutis)
        {
            summa = x + y;
            korrutis = x * y;
        }

        public static (int, int) Arvuta(int x, int y)
        {
            return (x + y, x* y);
        }

        public static void Vaheta(ref int x, ref int y)
        {
        (x, y) = (y, x);
        }
        public static 
    }
        class Program
        {
            static void Main(string[] args)
            {
                int arv = 77;
                MyFunctions.Tryki(arv, "arv");
                MyFunctions.Tryki(MyFunctions.Liida(7, 4), "tulemus");
                int s, k;
                MyFunctions.Arvuta(7, 5, out s, out k);
                MyFunctions.Vaheta(ref k, ref s);
            }
        }
    }