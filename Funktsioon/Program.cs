﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Spordipaev_klassidega
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = "spordipäeva protokoll.txt";
            string folder = ".";
            string[] failisisu;

            while (true)
            {
                try
                {
                    failisisu = File.ReadAllLines($"{folder}\\{filename}");
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("kus su fail on");
                    folder = Console.ReadLine();
                }
            }
            try
            {
                Console.WriteLine(string.Join("\n", failisisu));
                List<Sportlased> jooksjad = new List<Sportlased>();


                foreach (var i in failisisu)
                {
                    var read = i.Replace(", ", ",").Split(',');
                    jooksjad.Add(new Sportlased
                    {

                        Nimi = read[0],
                        Distants = int.Parse(read[1]),
                        Kiirus = double.Parse(read[2])

                    });
                }
                try
                {
                    foreach (var j in jooksjad)
                        Console.WriteLine(j.Nimi);


                }catch(Exception)
                {

                }


            } catch(Exception e)
            {
                Console.WriteLine($"jube nigel lugu{e.Message}");
            }
        }
    }
    class Sportlased
    {
        public string Nimi { get; set; }
        public double Distants { get; set; }
        public int Aeg;
        public double Kiirus;

    }

}


