﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klassid_2
{
    class Inimene
    {
        // public DateTime ilmus = DateTime.Now;    // sellised avaldised on lubatud. 
        static int loendur = 0;                     // klassi ja objekti elutsükkel
        public int Nr = ++loendur;



        static List<Inimene> inimesed = new List<Inimene>();

        public string Nimi = "";              //väljade puhul on vaikimisi kõigil asjadel mingi väärtus; value'del ja reference objektidel on väärtus 0; 
        public string IK = "000000000000";                      // klassis ei mängi avaldise järjekord rolli, ee on definitsioonide kogum;

        public static IEnumerable<Inimene> Inimesed = inimesed.AsEnumerable();
        /*
        private decimal _Palk;                      // ärireeglite kirjapilt selline _nt 
        public decimal GetPalk() => _Palk;

        public void SetPalk(decimal palk) => _Palk = palk > _Palk ? palk : _Palk;
        public decimal GetPalk()
        {
            return _Palk:
        }
        
        public void SetPalk(decimal palk)
        {
            if (palk > _Palk) _Palk = palk;
        }
        
        public  decimal _Palk
        {
            get => _Palk;
            set => _Palk = value > _Palk ? value : _Palk;
        }
        
        public decimal _Palk
        {
            get
            {
            return _Palk;
            }
            set
            {
            return _Palk = value > _Palk ? value : _Palk;
            }
                
        }
        */





        public Inimene()                    // voidi ei ole, klassi nimega sama nimi: constructor - meetod objekti loomiseks 
                                            // majapidamistoimingud uue objekti loomisel
                                            // klassi nimega meetod (ilma voidita)
                                            // käivitatakse automaatselt new ajal
                                            // eraldi seda välja kutsuda ei saa - ainult new lauses

            {
            inimesed.Add(this);
            }
        

        

        public DateTime Sünniaeg()
        {
            return 
                (IK.Length < 11) ? DateTime.Now :

                new DateTime(
                (IK[0] == '1' || IK[0] == '2' ? 1800 :
                IK[0] == '3' || IK[0] == '4' ? 1800 : 2000) +
                int.Parse(IK.Substring(1, 2)),
                int.Parse(IK.Substring(3, 2)),
                int.Parse(IK.Substring(5, 2))
                );
        }
        public override string ToString() { return $"inimene {Nimi} sündinud {Sünniaeg():dd.MMMM.yyyy}";}
       


    }
}
