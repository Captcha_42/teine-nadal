﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klassid_2
{
    class Program
    {
        enum Mast { Risti, Poti, Ärtu, Ruutu =4}
        [Flags]
        enum Tunnus { Suur= 1, Punane = 2, Puust = 4, Kolisev = 8}
        static void Main(string[] args)
        {
            Tunnus t = Tunnus.Suur | Tunnus.Kolisev;
            t |= Tunnus.Punane;
            Console.WriteLine(t);

            



            Inimene henn = new Inimene { Nimi = "Henn Sarv", IK = "35503070211" };
            Inimene ants = new Inimene { Nimi = "vana Ants" };
            Console.WriteLine(henn.Sünniaeg());
            Console.WriteLine(ants.Sünniaeg());
            /*
            henn.SetPalk(10000);
            Console.WriteLine(henn.GetPalk());
            henn.SetPalk(5000); // nätab ikka 10000, sest reegel on classis selline
            Console.WriteLine(henn.GetPalk());
            */
            //ants = null;
            ants = new Inimene { Nimi = "noor Ants" }; // sellel hetkel unustatakse vana ants ära

            int a = 7;
            int b = a * a;      // täidetav lause

            // ajutine muutuja näide: 
            //using (Inimene ajutine = new Inimene { Nimi = "Keegi"})
            //{
            // siin blokis on inimene olemas
            //}
            // siit alates seda muutujat enam ei ole

            foreach (var i in Inimene.Inimesed) Console.WriteLine(i);

            Console.WriteLine(Liida(4, 7)); // positsiooniline arvument
            Console.WriteLine(Liida(x : 4, y : 7)); // nimelised parameetrid
            Console.WriteLine(LiidaX(1, 2, 3, 4, 5, 6, 7)); // muutuva hulgaga parameetrid - parameetrite massiiviga. peale selle ei tohi lisada rohkem parameetreid
        }
        static int Liida(int x, int y)               // funktsioonid peavad olema unikaalse signatuuriga / päisega: Nimi ja parameetrid.
                                                     // näites on tehtud kahe erineva päiega funktsioonid.
        {
            return x + y;
        }

        //static int Liida(int x, int y, int z = 0) // z = 0 - optional parameeter - kahe parameetriga liida ja kolme parameetriga liida. 
        //{
        //    return x + y + z;
        //}
        static int Liida(int x, int y = 0, int z = 0) // z = 0 - optional parameeter - kahe parameetriga liida ja kolme parameetriga liida. 
        {
            return 4*x + 2*y + z;
        }

        
        
        static int LiidaX(int x, params int[] muud)
        {
            int sum = x;
            foreach (var s in muud) sum += s;
            return sum;
        }
        static double Liida(double d1, double d2)
        {
        return d1 +d2;
        }

    }
}
//kaks või enam samanimelist funkstiooni erinevate paramteerite arvu või tüübiga - funktsioonide overloadimine e. ülelaadimine
// funktsiooni overloadimisega peab olema täpne, et ei tekiks segadust funktsiooni valimisega.

    // overload sama nimega funktsioon/meetod erinevad paarameetrid (tüüp, arv, jrk)
    // optional - def väärtusega parameeter
    // params array - muutuva arv parameetreid (käsitletakse massiivina)
    // positsiooniline pöördumine                       :Liida(4,7,8)
    // nimeline pöödrumine - param jrk pole oluline     :Liida(z:8,x:4,y:7)
    // 